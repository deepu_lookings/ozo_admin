import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import ShopDetail from './screen/shopdetails/ShopDetail';
import OfferDetail from './screen/offerdetails/OfferDetails';
import UserDetail from './screen/userdetails/UserDetails';

import { Router, Route, Link, browserHistory } from 'react-router'
import * as serviceWorker from './serviceWorker';

import Request from './screen/request/Request';
import Offer from './screen/offer/Offer';
import Shop from './screen/shop/Shop';
import User from './screen/user/User';

import Login from './screen/login/';

import { Provider } from 'react-redux'
import configureStore from './store';

let store=configureStore()

export {store}
// ReactDOM.render(<Login />, document.getElementById('root'));



ReactDOM.render(

  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={Login} />
      <Route path="/requests" component={Request} />
      <Route path="/shops" component={Shop} />
      <Route path="/offers" component={Offer} />
      <Route path="/users" component={User} />
      <Route path="/shopdetail" component={ShopDetail} />
      <Route path="/userdetail" component={UserDetail} />
      <Route path="/offerdetail" component={OfferDetail} />
    </Router> </Provider>, document.getElementById('root'));



serviceWorker.unregister();

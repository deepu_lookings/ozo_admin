import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './Dashboard.css';
import {Tabs, TabList, Tab, PanelList, Panel} from 'react-tabtab';
import 'bootstrap/dist/css/bootstrap.min.css';


class Dashboard extends Component {
	render(){
		return(

		<Tabs customStyle={this.props.customStyle}>
			 <div className="container navbr">
	         <div className="nav-lft">
	            <a  className="" title="Lookings Soft">Dashboard</a>
	         </div>
	         <div className="nav-rit">
	            <a href="#home" id="nav-a1">Logout</a>
	         </div>
	   		</div>


        <TabList className="abcd">
	          <Tab>Requests</Tab>
	          <Tab>Offers</Tab>
	          <Tab>Shops</Tab>
	          
	     </TabList>
        <PanelList>
          <Panel>
            <table>
            <thead>
            <th>Title</th>
            <th>Description</th>
            <th>Action</th>
            </thead>
            <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td><a href=""><i className="fa fa-eye"></i></a> | <a href="">Status</a></td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td><a href=""><i className="fa fa-eye"></i></a> | <a href="">Status</a></td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td><a href=""><i className="fa fa-eye"></i></a> | <a href="">Status</a></td>
      </tr>
    </tbody>
            </table>
          </Panel>
          <Panel>
            Officiis commodi facilis optio eum aliquam.<br/>
            Tempore libero sit est architecto voluptate. Harum dolor modi deleniti animi qui similique facilis. Sit delectus voluptatem praesentium recusandae neque quo quod.
          </Panel>
          <Panel>
            Ut voluptas a voluptas quo ut dolorum.<br/>
            Dolorem sint velit explicabo sunt distinctio dolorem adipisci tempore.<br/>
            Est repellat quis magnam quo nihil amet et. Iste consequatur architecto quam neque suscipit.
                  
          </Panel>
        </PanelList>

      </Tabs>

			);
	}
}
export default Dashboard;
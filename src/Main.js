import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { browserHistory } from 'react-router';

class Main extends Component{

nextPath(path, result) {
    browserHistory.push({ pathname: path});
  }

	render(){

		return(
			<div>
         <div>
            <div className="navbr">
              <div className="nav">
                <div className="nav-lft">
                  <img className="img-logo" src={('img/ozo.png')} />
                  <a className="ozoTitle" href="/">Ozo Admin</a>
                </div>
                <div className="nav-rit">
                  <a id="nav-a1" href="javascript:window.location='/'"><i className="material-icons" title="Logout">&#xe8ac;</i></a>
                  
                </div>
              </div>
            </div>
            <div className="menu-bar">
              <div className="menu">
                <ul className="menu-ul">
                  <li className="menu-li"><a onClick={() => this.nextPath('/requests')}>Request</a></li>
                  <li className="menu-li"><a onClick={() => this.nextPath('/shops')}>Shops</a></li>
                  <li className="menu-li"><a onClick={() => this.nextPath('/offers')}>Offers</a></li>
                  <li className="menu-li"><a onClick={() => this.nextPath('/users')}>Users</a></li>
                </ul>
              </div>
            
          </div>
        </div>
       
      </div>
			);
		}
	}
export default Main;
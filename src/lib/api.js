import axios from 'axios'
import {store} from './../index'


function getToken(state)
{
 console.log("Current state",state)
 return state.login.user!=null?state.login.user.api_token:null
}

export default function request(type, url, params) {


let token = getToken(store.getState())
 console.log("token",token);
 axios.defaults.headers.common['Token']=token


  switch (type) {
    case 'get':
      return axios.get(url, { params: params })
        .then(function (response) {
          console.log("Server Response", response)
          return response.data
        })
        .catch(function (error) {
          console.log("Server Error", error)
          console.log("Server response", error.response)
         // window.location = '/';
          return error
        })
      break;
    case 'post':
      return axios.post(url, params)
        .then(function (response) {
          console.log(response)
          return response.data
        })
        .catch(function (error) {
          console.log("Server Error", error)
          console.log("Server response", error.response)
         // window.location = '/';
          return error
        })
  }
}

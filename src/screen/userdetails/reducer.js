

export default (state = {status:null }, action) => {
  switch (action.type) {

    case 'UPDATE_USER_STATUS':
      return { ...state, status: action.status }

    case 'FAILURE':
      return { ...state, shop: null, images: null }
    default:
      return state
  }
}


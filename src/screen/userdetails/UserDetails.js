import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';

import { connect } from 'react-redux';
import { statusupdate } from './action'

class UserDetails extends Component {

	constructor(props) {
		super(props);
		this.state = { user: [], status: this.props.location.state.is_blocked };
		console.log("users", this.props.location.state);
	}

	typeChange(id) {
		var type = ['Facebook', 'Google'];
		return type[id];
	}

	getuserStatus(status) {
		switch (status) {
			case 1:
				return <div className="buttons">
					<button className="reject btn" onClick={this.statusUpdate.bind(this, this.props.location.state.user_id, 2)}>Unblock</button>
				</div>
			case 2:
				return <div className="buttons">
					<button className="accept btn" onClick={this.statusUpdate.bind(this, this.props.location.state.user_id, 1)}>Blocked</button>

				</div>
			default:
				return <h1>defult</h1>
		}
	}

	statusUpdate(id, status) {
		this.setState({ status: status });
        this.props.dispatch(statusupdate({ id: id, status: status }));
	}

	render() {
		return (
			<div>
				<div className="navbr">
					<div className="nav">
						<div className="nav-lft">
							<img className="img-logo" src={('img/ozo.png')} />
							<a className="ozoTitle" href="/">Ozo Admin</a>
						</div>
						<div className="nav-rit">
							<a id="nav-a1"><i className="material-icons" title="Logout">&#xe8ac;</i></a>
							<a href="#" className="profile" id="nav-a1"><i className="material-icons" title="Logout">&#xe853;</i></a>
						</div>
					</div>
				</div>

				<div className="detail-bar">
					<div className="detail-menu">
						<a onClick={browserHistory.goBack}><i className="material-icons">arrow_back</i></a>
						<img className="img-responsive" src={this.props.location.state.photo} />
						<h4 className="">{this.props.location.state.first_name}</h4>
					</div>
					{this.getuserStatus(this.props.status ? this.props.status : this.state.status)}

				</div>


				<div className="dtls-content">
					<div className="col-lg-6 col-sm-6">
						<div className="group-dls-cont">
							<h5>Name</h5>
							<p>{this.props.location.state.first_name} {this.props.location.state.middle_name} {this.props.location.state.last_name}</p>
						</div>

						<div className="group-dls-cont">
							<h5>Email</h5>
							<p>{this.props.location.state.email}</p>
						</div>

						<div className="group-dls-cont">
							<h5>Phone</h5>
							<p>{this.props.location.state.phone}</p>
						</div>

						<div className="group-dls-cont">
							<h5>User Type</h5>
							<p>{this.typeChange(this.props.location.state.auth_type)}</p>
						</div>
					</div>

				</div>
			</div>

		);
	}
}

function mapStatetoProps(state) {
    return {
        status: state.shopdetails.status
    }
}

export default connect(mapStatetoProps)(UserDetails);
import Api from '../../lib/api'
import { UPDATEUSERSTATUS_API } from './../../config/endpoint'

export const statusupdate = (params) => {


  return function (dispatch) {

    Api('post', UPDATEUSERSTATUS_API, params)
      .then((response) => {
        console.log("responses", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'UPDATE_USER_STATUS', status: response.data })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}
import Api from '../../lib/api'
import { REQUEST_API,SEARCHREQUEST_API } from './../../config/endpoint'

export const requestlist = (params) => {
  return function (dispatch) {

    Api('get', REQUEST_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_REQUESTS', data: response.data.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}


export const searchresult = (params) => {
  return function (dispatch) {

    Api('get', SEARCHREQUEST_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_REQUESTS', data: response.data.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}


export const sortlist = (params) => {
  return function (dispatch) {

    Api('get', REQUEST_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_REQUESTS', data: response.data.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Main from '../../Main';
import { browserHistory } from 'react-router';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import { requestlist,sortlist,searchresult,sea } from './action'

class Request extends Component {
  constructor() {
    super();
    this.state = { data: [], offset: 0, input: '', order: '', select: '' };
    this.loadDataFromServer = this.loadDataFromServer.bind(this)
  }

  componentDidMount() {
    return this.loadDataFromServer(1);
  }

  componentWillReceiveProps(props) {
    console.log("new props", props.data);
  }

  loadDataFromServer(page) {
    this.props.dispatch(requestlist({ page: page}))
  }

 handlePageClick = (data) => {

    
    console.log("data",data)
    let selected = data.selected;
    let offset = Math.ceil(selected * this.props.perPage);

if(this.state.input =='' && this.state.order =='' ){
    this.setState({ offset: offset }, () => {
      this.loadDataFromServer(selected + 1);
    });
  }
  else if(this.state.input !='' && this.state.order =='' )
  {
    this.setState({ offset: offset }, () => {
      this.searchresult(selected+1);
    });
    
  }
  else
  {
  var value = this.state.order?this.state.order.split(','):'created_at,desc';
  var order = value[0];
  var by = value[1];
   this.setState({ offset: offset }, () => {
      this.props.dispatch(sortlist({ page: selected+1,order:order,by:by,tag:this.state.input }));
    }); 
  }
  };

    searchresult(pages) {
    var tag = this.state.input;
     this.props.dispatch(searchresult({ page: pages,tag:tag }))
  }
  

  handleinputChange = (e) => {
    this.setState({
      input: e.target.value
    },()=>this.props.dispatch(searchresult({ page: 1,tag:this.state.input })))
   
     
  }

handledropdownChange = (e) => {
  this.setState({
    order: e.target.value
  },()=>
  {
  var value = this.state.order.split(',');;
  var order = value[0];
  var by = value[1];
  console.log("order",order);
  return this.props.dispatch(sortlist({ page: 1,order:order,by:by,tag:this.state.input }));
})   
}






  statusChange(id, offerid) {
    var status = ['Waiting', 'Approved', 'Rejected'];
    if (offerid == null)
      return status[id];
    else
      return status[offerid];
  }

  nextPath(path, result) {
    browserHistory.push({ pathname: path, state: result });
  }

  name(shopid, name, title) {
    if (shopid == null) {
      return name;
    }
    else {
      return title;

    }
  }

  description(shopid, description, offer_description) {
    if (shopid == null) {
      return description;
    }
    else {
      return offer_description;

    }
  }

  option(shopid, result) {
    if (shopid == null) {
      return <button onClick={() => this.nextPath("/shopdetail", result)}><i className="fa fa-eye"></i></button>;
    }
    else {
      return <button onClick={() => this.nextPath("/offerdetail", result)}><i className="fa fa-eye"></i></button>;
    }
  }



  render() {
    return (
      <div>
        <Main />
        <div className="options">
          <input type="text" onChange={this.handleinputChange} placeholder="Search" /><a onClick={this.searchresult.bind(this,1)}><i className="material-icons">&#xe8b6;</i></a>
          <select value={this.state.select} onChange={this.handledropdownChange}>
            <option value="created_at,asc">By Date(Ascending)</option>
            <option value="created_at,desc">By Date(Descending)</option>
            <option value="title,asc">By A-Z</option>
            <option value="title,desc" >By Z-A</option>
          </select>

        </div>
        
          <div className="tabular">
            <table>
              <thead>
                <th>Title</th>
                <th >Description</th>
                <th className="align-right">Status</th>
                <th className="align-right">Options</th>
              </thead>
              <tbody>
                {
                  this.props.data && this.props.data.map((result) => {
                    return <tr> <td key={result.id}> {this.name(result.shop_id, result.name, result.title)} </td>
                      <td > {this.description(result.shop_id, result.description, result.offer_description)} </td> <td className="align-right">{this.statusChange(result.status, result.offer_status)}</td>
                      <td className="viewer align-right">{this.option(result.shop_id, result)}</td>
                    </tr>
                  })

                }
              </tbody>
            </table>
          </div>
          <div className="paginater">
            <ReactPaginate previousLabel={<i className='glyphicon glyphicon-menu-left' />}
              nextLabel={<i className='glyphicon glyphicon-menu-right' />}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={this.props.pagecount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={4}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"} />
          </div>
        
      </div>
    );
  }
}

function mapStatetoProps(state) {
  return {
    data: state.requestlist.data,
    pagecount: state.requestlist.pagecount,
    user:state.login.user
  }
}

export default connect(mapStatetoProps)(Request);



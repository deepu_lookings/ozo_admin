export default (state = { data: null, pagecount: 0 }, action) => {
  switch (action.type) {
    case 'GET_USERS':
      return { ...state, data: action.data, pagecount: action.pagecount }
    case 'FAILURE':
      return { ...state, data: null, pagecount: 0 }
    default:
      return state
  }
}


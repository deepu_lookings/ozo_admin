import Api from '../../lib/api'
import { USER_API, SEARCHUSER_API, SORTUSER_API } from './../../config/endpoint'

export const userlist = (params) => {
  return function (dispatch) {

    Api('get', USER_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_USERS', data: response.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}


export const searchuserlist = (params) => {
  return function (dispatch) {

    Api('get', SEARCHUSER_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_USERS', data: response.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}

export const sortuserlist = (params) => {
  return function (dispatch) {

    Api('get', SORTUSER_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_USERS', data: response.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}

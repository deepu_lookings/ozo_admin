import Api from '../../lib/api'
import { SHOP_API,SORTSHOP_API,SEARCHSHOP_API } from './../../config/endpoint'

export const shoplist = (params) => {
  return function (dispatch) {

    Api('get', SHOP_API, params)
      .then((response) => {
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_SHOPS', data: response.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })         
        }
      })
  }
}

export const searchshoplist = (params) => {
  return function (dispatch) {

    Api('get', SEARCHSHOP_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_SHOPS', data: response.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}

export const sortshoplist = (params) => {
  return function (dispatch) {

    Api('get', SORTSHOP_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_SHOPS', data: response.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}

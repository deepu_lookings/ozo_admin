import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ShopDetail from '../shopdetails/ShopDetail';
import Main from '../../Main';
import { browserHistory } from 'react-router';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import { shoplist,searchshoplist,sortshoplist } from './action'

class Shop extends Component {

  constructor(props) {
    super(props);
    console.log("Props", this.props);
    this.state = { data: [], offset: 0, input: '', order: '', select: '' };
    this.loadDataFromServer = this.loadDataFromServer.bind(this)
  }

  componentDidMount() {
    this.loadDataFromServer(1);
  }

  componentWillReceiveProps(props) {
    console.log("new props", props.data);
  }

  loadDataFromServer(page) {
    this.props.dispatch(shoplist({ page: page,Token:this.props.user }))
  }

  handlePageClick = (data) => {

    
    console.log("data",data)
    let selected = data.selected;
    let offset = Math.ceil(selected * this.props.perPage);
if(this.state.input =='' && this.state.order =='' ){
    this.setState({ offset: offset }, () => {
      this.loadDataFromServer(selected + 1);
    });
  }
  else if(this.state.input !='' && this.state.order =='' )
  {
    this.setState({ offset: offset }, () => {
      this.searchresult(selected+1);
    });
    
  }
  else
  {
  var value = this.state.order?this.state.order.split(','):'created_at,desc';
  var order = value[0];
  var by = value[1];
   this.setState({ offset: offset }, () => {
      this.props.dispatch(sortshoplist({ page: selected+1,order:order,by:by,tag:this.state.input }));
    }); 
  }
  };

  statusChange(id) {
    var status = ['Waiting', 'Approved', 'Rejected'];
    return status[id];
  }

  nextPath(path, result) {
    browserHistory.push({ pathname: path, state: result });
  }

   searchresult(pages) {
    var tag = this.state.input;
     this.props.dispatch(searchshoplist({ page: pages,tag:tag }))
  }
  

  handleinputChange = (e) => {
    this.setState({
      input: e.target.value
    })
    return this.props.dispatch(searchshoplist({ page: 1,tag:this.state.input }));
     
  }

handledropdownChange = (e) => {
  this.setState({
    order: e.target.value
  },()=>
  {
  var value = this.state.order.split(',');;
  var order = value[0];
  var by = value[1];
  console.log("order",order);
  return this.props.dispatch(sortshoplist({ page: 1,order:order,by:by,tag:this.state.input }));
})   
}






  render() {
    return (
      <div>
        <Main />
        <div className="options">
          <input type="text" onChange={this.handleinputChange} placeholder="Search" /><a onClick={this.searchresult.bind(this,1)}><i className="material-icons">&#xe8b6;</i></a>
          <select value={this.state.select} onChange={this.handledropdownChange}>
            <option value="created_at,asc">By Date(Ascending)</option>
            <option value="created_at,desc">By Date(Descending)</option>
            <option value="name,asc">By A-Z</option>
            <option value="name,desc" >By Z-A</option>
          </select>

        </div>
          <div className="tabular ">
            <table>
              <thead>
                <th>Title</th>
                <th>Description</th>
                <th className="align-right">Status</th>
                <th className="align-right">Options</th>
              </thead>
              <tbody>
                {
                  this.props.data && this.props.data.map((result, index) => {
                    return <tr><td key={index}>{result.name} </td>
                      <td>{result.description}</td><td className="align-right">{this.statusChange(result.status)}</td>
                      <td className="viewer align-right"><button onClick={() => this.nextPath('/shopdetail', result)}><i className="fa fa-eye"></i></button></td>
                    </tr>
                  })

                }
              </tbody>
            </table>
          </div>
          <div className="paginater">
            <ReactPaginate previousLabel={<i className='glyphicon glyphicon-menu-left' />}
              nextLabel={<i className='glyphicon glyphicon-menu-right' />}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={this.props.pagecount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"} />
          </div>
        </div>
      
    );
  }
}

function mapStatetoProps(state) {
  return {
    data: state.shoplist.data,
    pagecount: state.shoplist.pagecount,
    user:state.login.user
  }
}

export default connect(mapStatetoProps)(Shop);

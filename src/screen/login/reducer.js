export default (state = { user: null, loading: false, loginError: false }, action) => {
  switch (action.type) {
    case 'LOGIN':
      return { ...state, loading: true }
    case 'LOGIN_SUCCESS':
      return { ...state, loading: false, user: action.user, loginError: false }
    case 'LOGIN_FAILURE':
      return { ...state, loading: false, loginError: true }
    default:
      return state
  }
}
import Api from '../../lib/api'
import { LOGIN_API } from './../../config/endpoint'

export const login = (params) => {
  return function (dispatch) {

    dispatch({ type: 'LOGIN' })
    Api('post', LOGIN_API, params)
      .then((response) => {

        console.log("Paramssss",params)

        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'LOGIN_SUCCESS', user:response.data })
        }
        else {
          dispatch({ type: 'LOGIN_FAILURE' })
        }
      })
  }
}

import React, { Component } from 'react';
import logo from '../../logo.svg';
import { login } from './action'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router';


class App extends Component {

   constructor(props) {
      super(props);
      console.log("Props", this.props);
      this.state = { data: [], email: '', secret: '', erroremail: [], errorkey: [],login:'Login',logerr:'' };
      this.nextPath = this.nextPath.bind(this)

   }

   componentWillReceiveProps(props) {

      if (props.loginStatus == true) {

         this.setState({ login: 'Logging..!'});
      }
      else if(props.loginError == true)
      {
         this.setState({ login: 'Login',logerr:'Oops.. Something went wrong.! '});
      }
      if (props != this.props) {
         
         if (!props.loading && !props.loginError && true && props.user) {
            this.setState({ logerr:''});
            this.nextPath(`requests`, {})
         }
      }
   }

   nextPath(path, result) {
      
      setTimeout(function(){
         browserHistory.push({ pathname: path, state: result });
      },3000)
      
   }

   handlemailChange = (e) => {
      this.setState({
         email: e.target.value
      })
   }

   handlsecretkeyChange = (e) => {
      this.setState({
         secret: e.target.value
      })
   }

   handleSubmit() {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      var email = this.state.email;
      var secret = this.state.secret;

console.log("email",email)
console.log("secret",secret)

      if (!email.match(reg)) {
         this.setState({ erroremail: 'Please enter valid email.!' })
         return false;
      }
      else if (secret == "") {
         this.setState({ erroremail: '', errorkey: 'Please enter valid key.!' })
         return false;
      }
      else {
         
         this.setState({ erroremail: '', errorkey: '' })
         this.props.dispatch(login({ auth_id: secret, email: email, type: 3,headers: {
     'Access-Control-Allow-Origin': '*'}
     }))
      }
   }

   render() {
      return (
         <div className="background">
            <div className="container">
               <div className="outer-content">
                  <div className="content">
                     <div className="left">
                        <img src={('img/ozo.png')} />
                        <p className="admin">Admin</p>
                        <h2>Welcome to Ozo</h2>
                        <p>Please confirm that you are visiting admin panelslslsl </p>
                     </div>
                     <div className="right">
                        <h2>Login</h2>
                        <p>Welcome! Please confirm that you are visiting admin panel. <a href="#">https://www.expale.com</a> </p>
                        <form>
                           <div className="ip-contn">
                              <span>Email:</span>
                              <input type="email" onChange={this.handlemailChange} name="email" id="email" />
                              <span className="error-email">{this.state.erroremail}</span>

                           </div>
                           <div className="ip-contn">
                              <span>Secret Key:</span>
                              <input type="password" onChange={this.handlsecretkeyChange} name="key" id="key" />
                              <span className="error-email">{this.state.errorkey}</span>
                           </div>
                           <p>{this.state.logerr}</p>
                           <div className="login">
                              <input type="button"  onClick={this.handleSubmit.bind(this)} name="submit" value={this.state.login} id="submit" />
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      );
   }
}

function mapStatetoProps(state) {
   return {
      loginStatus: state.login.loading,
      loginError: state.login.loginError,
      user:state.login.user
   }
}

export default connect(mapStatetoProps)(App);

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Carousel } from 'react-responsive-carousel';
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import GoogleMapReact from 'google-map-react';

import { withRouter } from "react-router";
import { browserHistory } from 'react-router';

import { connect } from 'react-redux';
import { shopbyId,offerimagelist, statusupdate } from './action'


class OfferDetails extends Component {

    constructor(props) {
        super(props);
        this.state = { shop: [], images: [], request: [], status: this.props.location.state.offer_status };
        console.log("Params", this.props.location.state);
    }

    componentDidMount() {
        this.props.dispatch(shopbyId({ id: this.props.location.state.shop_id }));
        this.props.dispatch(offerimagelist({ offer_id: this.props.location.state.offer_id }));
    }

    componentWillReceiveProps(props) {
        console.log("new props", props);
    }

    statusChange(id) {
        var status = ['Waiting', 'Approved', 'Rejected'];
        return status[id];
    }

    getofferStatus(status) {
        switch (status) {
            case 0:
                return <div className="buttons">
                    <button className="accept btn" onClick={this.statusUpdate.bind(this, this.props.location.state.offer_id, 1)}>Accept</button>
                    <button className="reject btn" onClick={this.statusUpdate.bind(this, this.props.location.state.offer_id, 2)}>Reject</button>
                </div>
            case 1:
                return <div className="buttons">
                    <button className="reject btn" onClick={this.statusUpdate.bind(this, this.props.location.state.offer_id, 2)}>Reject</button>
                </div>
            case 2:
                return <div className="buttons">
                    <button className="accept btn" onClick={this.statusUpdate.bind(this, this.props.location.state.offer_id, 1)}>Accept</button>

                </div>
            default:
                return <h1>defult</h1>

        }
    }

    statusUpdate(id, status) {
        this.setState({ status: status });
        this.props.dispatch(statusupdate({ id: id, status: status }));
    }


    render() {

        const MY_API = 'AIzaSyCc3zoz5TZaG3w2oF7IeR-fhxNXi8uywNk'
        const longitude = this.props.shop ? this.props.shop.longitude : 0
        const latitude = this.props.shop ? this.props.shop.latitude : 0

        var url = `https://www.google.com/maps/embed/v1/place?key=${MY_API}&q=${longitude},${latitude}`
        var percentage = Math.round(100 - ((this.props.location.state.offer_price / this.props.location.state.price) * 100));
        
        return (
            <div>
                <div className="navbr">
                    <div className="nav">
                        <div className="nav-lft">
                            <img className="img-logo" src={('img/ozo.png')} />
                            <a className="ozoTitle" href="/">Ozo Admin</a>
                        </div>
                        <div className="nav-rit">
                            <a id="nav-a1" href="javascript:window.location='/'"><i className="material-icons" title="Logout">&#xe8ac;</i></a>
                        </div>
                    </div>
                </div>

                <div className="detail-bar">
                    <div className="detail-menu">
                        <a onClick={browserHistory.goBack}><i className="material-icons">arrow_back</i></a>
                        <img className="img-responsive" src={this.props.location.state.image && JSON.parse(this.props.location.state.image).name} />
                        <h4 className="">{this.props.location.state.title}</h4>
                    </div>
                    {this.getofferStatus(this.props.status ? this.props.status : this.state.status)}
                </div>

                <div className="dtls-content">
                    <div className="col-lg-6 col-sm-6">
                        <div className="group-dls-cont">
                            <h5>About</h5>
                            <p>{this.props.location.state.offer_description}</p>
                        </div>

                        <div>
                            <h3>Shop </h3>
                            <div className="col-lg-6 col-sm-6 offerset">

                                <div className="group-dls-cont">
                                    {this.props.shop && <h3>{this.props.shop.name}</h3>}
                                </div>

                                <div className="group-dls-cont">
                                    <h5>Address</h5>
                                    <p>Kakkanad Road, Padamugal, Vazhakkala, Kochi, Kerala - 682 021</p>
                                </div>

                                <div className="group-dls-cont">
                                    <h5>Contact No.</h5>
                                    {this.props.shop && <p>{this.props.shop.mobno}</p>}
                                </div>

                            </div>
                            <div className="col-lg-6 col-sm-6">

                                <div className="group-dls-cont ">

                                    <h5>Location</h5>
                                    <iframe frameBorder="0" style={{ width: "100%", height: "450" }}
                                        src={url}>
                                    </iframe>

                                </div>
                            </div>
                        </div>


                    </div>

                    <div className="col-lg-6 col-sm-6">

                        <div className="col-lg-4 col-sm-6">

                            <div className="group-dls-cont">
                                <h5>Original Price</h5>
                                <p><span>Rs. </span>{this.props.location.state.price}<span>/-</span></p>
                            </div>
                        </div>
                        <div className="col-lg-4 col-sm-6">

                            <div className="group-dls-cont">
                                <h5>Offer Price</h5>
                                <p><span>Rs. </span>{this.props.location.state.offer_price}<span>/-</span></p>
                            </div>
                        </div>
                        <div className="col-lg-4 col-sm-6">

                            <div className="group-dls-cont">
                                <h5>Percentage</h5>
                                <p>{this.props.location.state.percentage}<span>% off</span></p>
                            </div>
                        </div>

                        <div className="col-lg-12 col-sm-12">

                            <div className="group-dls-cont img-fit">
                                <h5>Image Gallery</h5>
                                <Carousel autoPlay={true} infiniteLoop={true}>
                                    {
                                        this.props.images && this.props.images.map((result) => {
                                            return <div><img src={result} />
                                            </div>
                                        })
                                    }
                                </Carousel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

function mapStatetoProps(state) {
    return {
        shop: state.offerdetails.shop,
        images: state.offerdetails.images,
        status: state.offerdetails.status
    }
}

export default connect(mapStatetoProps)(withRouter(OfferDetails));
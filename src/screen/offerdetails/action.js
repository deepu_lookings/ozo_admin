import Api from '../../lib/api'
import { SHOPBYID_API, OFFERIMAGES_API, UPDATEOFFERSTATUS } from './../../config/endpoint'


export const shopbyId = (params) => {
  return function (dispatch) {

    Api('get', SHOPBYID_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_SHOP', shop: response.data })
          console.log("gets", response.data)
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}

export const offerimagelist = (params) => {


  return function (dispatch) {

    Api('get', OFFERIMAGES_API, params)
      .then((response) => {
        console.log("responses", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_OFFER_IMAGES', images: response.data })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}



export const statusupdate = (params) => {


  return function (dispatch) {

    Api('post', UPDATEOFFERSTATUS, params)
      .then((response) => {
        console.log("responses", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'UPDATE_OFFER_STATUS', status: response.data })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}
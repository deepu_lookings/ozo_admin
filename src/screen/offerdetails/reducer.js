

export default (state = { shop: null, images: null,status:null }, action) => {
  switch (action.type) {
    case 'GET_SHOP':
      return { ...state, shop: action.shop }

    case 'GET_OFFER_IMAGES':
      return { ...state, images: action.images }

    case 'UPDATE_OFFER_STATUS':
      return { ...state, status: action.status }

    case 'FAILURE':
      return { ...state, shop: null, images: null }
    default:
      return state
  }
}


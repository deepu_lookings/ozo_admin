import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Carousel } from 'react-responsive-carousel';
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import GoogleMapReact from 'google-map-react';

import { withRouter } from "react-router";
import { browserHistory } from 'react-router';

import { connect } from 'react-redux';
import { offersshopbyId,shopimagelist, statusupdate } from './action'



class ShopDetails extends Component {

    constructor(props) {
        super(props);
        this.state = { data: [], offers: [], request: [], status: this.props.location.state.status };
        console.log("History Params", this.props.location.state);
    }

    componentDidMount() {

        this.props.dispatch(offersshopbyId({ id: this.props.location.state.id }));
        this.props.dispatch(shopimagelist({ shop_id: this.props.location.state.id }));
    }

     componentWillReceiveProps(props) {
        console.log("new props", props);
    }


    statusChange(id) {
        var status = ['Waiting', 'Approved', 'Rejected'];
        return status[id];
    }

    getshopStatus(status) {
        switch (status) {
            case 0:
                return <div className="buttons">
                    <button className="accept btn" onClick={this.statusUpdate.bind(this, this.props.location.state.id, 1)}>Accept</button>
                    <button className="reject btn" onClick={this.statusUpdate.bind(this, this.props.location.state.id, 2)}>Reject</button>
                </div>
            case 1:
                return <div className="buttons">
                    <button className="reject btn" onClick={this.statusUpdate.bind(this, this.props.location.state.id, 2)}>Reject</button>
                </div>
            case 2:
                return <div className="buttons">
                    <button className="accept btn" onClick={this.statusUpdate.bind(this, this.props.location.state.id, 1)}>Accept</button>

                </div>
            default:
                return <h1>defult</h1>

        }
    }

    statusUpdate(id, status) {
        this.setState({ status: status });
        this.props.dispatch(statusupdate({ id: id, status: status }));
    }

    render() {
        const MY_API = 'AIzaSyCc3zoz5TZaG3w2oF7IeR-fhxNXi8uywNk'
        const longitude = this.props.location.state.longitude
        const latitude = this.props.location.state.latitude
        var url = `https://www.google.com/maps/embed/v1/place?key=${MY_API}&q=${longitude},${latitude}`

        // var logo = 'http://192.168.0.2/p1/storage/app/images/shop/'+this.props.location.state.id+'/'+this.props.location.state.image;
        var logo = this.props.location.state.image;
        if (logo == null) {
            logo = "img/back3.png";
        }

        return (
            <div>
                <div className="navbr">
                    <div className="nav">
                        <div className="nav-lft">
                            <img className="img-logo" src={('img/ozo.png')} />
                            <a className="ozoTitle" href="/">Ozo Admin</a>
                        </div>
                        <div className="nav-rit">
                            <a id="nav-a1" href="javascript:window.location='/'"><i className="material-icons" title="Logout">&#xe8ac;</i></a>
                        </div>
                    </div>
                </div>

                <div className="detail-bar">
                    <div className="detail-menu">
                        <a onClick={browserHistory.goBack}><i className="material-icons">arrow_back</i></a>
                        <img className="img-responsive" src={logo} />
                        <h4 className="">{this.props.location.state.name}</h4>
                    </div>
                    {this.getshopStatus(this.props.status ? this.props.status : this.state.status)}
                </div>
                <div className="dtls-content">
                    <div className="col-lg-6 col-sm-6">
                        <div className="group-dls-cont">
                            <h5>About</h5>
                            <p>{this.props.location.state.description}</p>
                        </div>
                        <div className="col-lg-12 col-sm-12">
                            <div className="group-dls-cont img-fit">
                                <h5>Image Gallery</h5>

                                <Carousel autoPlay={true} infiniteLoop={true}>

                                    {
                                        this.props.images && this.props.images.map((result) => {
                                            return <div><img src={result} /></div>
                                        })
                                    }
                                </Carousel>
                            </div>
                        </div>
                        <div className="group-dls-cont">
                            <h5>Offers</h5>
                            <table>
                                <thead>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Offer Price(%)</th>
                                    <th>Status</th>
                                </thead>
                                <tbody>
                                    {
                                        this.props.offers ? this.props.offers.map((result) => {
                                            return <tr> <td key={result.id}> {result.title} </td>
                                                <td > {result.price} </td> <td>{result.offer_price}</td>
                                                <td className="viewer">{this.statusChange(result.status)}</td>
                                            </tr>
                                        }):null

                                    }

                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div className="col-lg-6 col-sm-6">

                        <div className="group-dls-cont">
                            <h5>Address</h5>
                            <p>#CS3, 3rd floor, Heavenly Plaza,Vazhakkala, Kochi, Kerala - 682 021</p>
                        </div>

                        <div className="group-dls-cont">


                            <h5>Contact No.</h5>
                            <p>{this.props.location.state.mobno}</p>

                        </div>

                        <div className="group-dls-cont map">

                            <h5>Location</h5>
                            <iframe frameBorder="0" style={{ width: "100%", height: "450" }}
                                src={url}>
                            </iframe>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


function mapStatetoProps(state) {
    return {
        offers: state.shopdetails.offers,
        images: state.shopdetails.images,
        status: state.shopdetails.status
    }
}

export default connect(mapStatetoProps)(withRouter(ShopDetails));

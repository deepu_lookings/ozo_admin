import Api from '../../lib/api'
import { OFFERSBYSHOPID_API,SHOPIMAGES_API,UPDATESHOPSTATUS } from './../../config/endpoint'


export const offersshopbyId = (params) => {
  return function (dispatch) {

    Api('get', OFFERSBYSHOPID_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_OFFERS', offers: response.data.data })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}

export const shopimagelist = (params) => {


  return function (dispatch) {

    Api('get', SHOPIMAGES_API, params)
      .then((response) => {
        console.log("responses", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_SHOP_IMAGES', images: response.data })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}



export const statusupdate = (params) => {


  return function (dispatch) {

    Api('post', UPDATESHOPSTATUS, params)
      .then((response) => {
        console.log("responses", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'UPDATE_SHOP_STATUS', status: response.data })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}
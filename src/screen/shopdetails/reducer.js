

export default (state = { offers: null, images: null,status:null }, action) => {
  switch (action.type) {
    case 'GET_OFFERS':
      return { ...state, offers: action.offers }

    case 'GET_SHOP_IMAGES':
      return { ...state, images: action.images }

    case 'UPDATE_SHOP_STATUS':
      return { ...state, status: action.status }

    case 'FAILURE':
      return { ...state, shop: null, images: null }
    default:
      return state
  }
}


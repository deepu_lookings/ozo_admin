import Api from '../../lib/api'
import { OFFER_API, SEARCHOFFER_API, SORTOFFER_API } from './../../config/endpoint'

export const offerlist = (params) => {
  return function (dispatch) {

    Api('get', OFFER_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_OFFERS', data: response.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}

export const searchofferlist = (params) => {
  return function (dispatch) {

    Api('get', SEARCHOFFER_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_OFFERS', data: response.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}

export const sortofferlist = (params) => {
  return function (dispatch) {

    Api('get', SORTOFFER_API, params)
      .then((response) => {
        console.log("response", response)
        if (response.status_code && response.status_code == 200) {
          dispatch({ type: 'GET_OFFERS', data: response.data, pagecount: response.pagecount })
        }
        else {
          dispatch({ type: 'FAILURE' })
        }
      })
  }
}

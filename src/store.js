import {createStore,applyMiddleware,combineReducers,compose} from 'redux'
import thunk from 'redux-thunk';
import loginReducer from './screen/login/reducer';
import shopReducer from './screen/shop/reducer';
import offerReducer from './screen/offer/reducer';
import requestReducer from './screen/request/reducer';
import userReducer from './screen/user/reducer';
import offerdetailReducer from './screen/offerdetails/reducer';
import shopdetailReducer from './screen/shopdetails/reducer';
import userdetailReducer from './screen/userdetails/reducer';

import {logger} from 'redux-logger'

const Reducerslist=combineReducers(
	{	login:loginReducer,
		shoplist:shopReducer,
		offerlist:offerReducer,
		requestlist:requestReducer,
		userlist:userReducer,
		offerdetails:offerdetailReducer,
		shopdetails:shopdetailReducer,
		userdetails:userdetailReducer
	})

export default function configureStore() {
 return createStore(
  Reducerslist,
   applyMiddleware(thunk,logger)
 );
}
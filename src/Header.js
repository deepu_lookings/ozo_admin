import React, { Component } from 'react';
import ReactDOM from 'react-dom';


let current;

class Header extends Component {

constructor(props) {
    super(props);
    this.state = {current: 'r'};
    this.requestClick = this.requestClick.bind(this);
    this.offerClick = this.offerClick.bind(this);
    this.shopClick = this.shopClick.bind(this);
    this.userClick = this.userClick.bind(this);
  }

  requestClick(){
   current =<Request/>;
  }
  offerClick(){
   current =<Offer/>;
  }
  shopClick(){
   current =<Shop/>;
  }
  userClick(){
   current =<User/>;
  }

   render() {
      return (
         <div>
         <div className="navbr">
         <div className="nav">
            <div className="nav-lft">
            <img className="img-logo" src={('img/ozo.png')} />
               <a className="ozoTitle" href="/">Ozo Admin</a>
            </div>
            <div className="nav-rit">
               <a id="nav-a1"><i className="material-icons" title="Logout">&#xe8ac;</i></a>
               <a className="profile" id="nav-a1"><i className="material-icons" title="Logout">&#xe853;</i></a>
            </div>
         </div>
        </div>
        <div className="menu-bar">
        <div className="menu">
        <ul className="menu-ul">
        <li className="menu-li"><a onClick={this.requestClick}>Request</a></li>
        <li className="menu-li"><a onClick={this.shopClick} >Shops</a></li>
        <li className="menu-li"><a onClick={this.offerClick} >Offers</a></li>
        <li className="menu-li"><a onClick={this.userClick} >Users</a></li>
        </ul>
        </div>
        <div className="options">
        <input type="text" placeholder="Search"/><i className="material-icons">&#xe8b6;</i>
        <select>
         <option value="byDate">By Date</option>
         <option value="byAZ">By A-Z</option>
         <option value="byZA">By Z-A</option>
        </select>

        </div>
        </div>
        {current}
        </div>
      );
   }
}

export default Header;

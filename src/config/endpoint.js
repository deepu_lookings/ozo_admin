const HOST 					= 'http://192.168.0.11/ozo_api/public/api/admin/';

const LOGIN_API 			= HOST + 'signin';

const REQUEST_API 			= HOST + 'get_requests';
const SEARCHREQUEST_API 	= HOST + 'searchrequest';


const USER_API 				= HOST + 'get_users';
const UPDATEUSERSTATUS_API 	= HOST + 'updateuserblock';
const SEARCHUSER_API 		= HOST + 'searchuser';
const SORTUSER_API			= HOST + 'sortuser';

const SHOP_API 				= HOST + 'get_shops';
const SEARCHSHOP_API 		= HOST + 'searchshop';
const SORTSHOP_API			= HOST + 'sortshop';
const OFFERSBYSHOPID_API 	= HOST + 'getOffersByShopId';
const SHOPIMAGES_API 		= HOST + 'shopImages';
const UPDATESHOPSTATUS 		= HOST + 'updateShopStatus';

const OFFER_API 			= HOST + 'get_offers';
const SEARCHOFFER_API 		= HOST + 'searchoffer';
const SHOPBYID_API 			= HOST + 'getShopById';
const OFFERIMAGES_API 		= HOST + 'offerImages';
const UPDATEOFFERSTATUS 	= HOST + 'updateOfferStatus';
const SORTOFFER_API			= HOST + 'sortoffer';

export { LOGIN_API, 
		 SHOP_API, 
		 REQUEST_API, 
		 USER_API, 
		 OFFER_API, 
		 SHOPBYID_API, 
		 OFFERIMAGES_API, 
		 UPDATEOFFERSTATUS,
		 OFFERSBYSHOPID_API,
		 SHOPIMAGES_API,
		 UPDATESHOPSTATUS,
		 UPDATEUSERSTATUS_API,
		 SEARCHSHOP_API,
		 SEARCHOFFER_API,
		 SORTOFFER_API,
		 SORTSHOP_API,
		 SORTUSER_API,
		 SEARCHUSER_API,
		 SEARCHREQUEST_API
	 }